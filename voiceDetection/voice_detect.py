import speech_recognition as sr
import os

class voiceDetect():

    def __init__(self):
        pass

    # function to take in a .wav file and output text
    def wavToText(self,file):
        
        # create an instance of the speech_recognition class
        r = sr.Recognizer()

        with sr.AudioFile(file) as source:
            # listen for the data (load audio to memory)
            audio_data = r.record(source)
            # recognize (convert from speech to text)
            text = r.recognize_google(audio_data)
        
        return text
