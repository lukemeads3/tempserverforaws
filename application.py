from flask import Flask, request, send_file
from datetime import datetime
from logger import log
import time
import base64
import os
import sys
import requests
import json
from PyDictionary import PyDictionary

dictionary = PyDictionary()

log = log()
application = Flask(__name__)
log.startup()

from objectDetection.yolo_opencv import objectDetect
from voiceDetection.voice_detect import voiceDetect
# from chatbot.chatbot_class import chatBot
from objectDetection.read_video import videoDetect

# Create Instance of Critical objecs
voice = voiceDetect()
#url = 'http://0.0.0.0:5001/chatbot'
url = 'http://ec2-3-139-160-148.us-east-2.compute.amazonaws.com:5000/chatbot'

mobileNetOptions = []
askOptions = []


@application.route('/')
def hello_world():
    log.endpointReached('hello_world', request.remote_addr)
    return "<h1>hello world iseek.</h1>"


@application.route('/time')
def send_time():
    log.endpointReached('time', request.remote_addr)
    print('hello world')
    return {'time': time.time()}


@application.route('/text', methods=['POST', 'GET'])
def recieveText():
    log.endpointReached('text', request.remote_addr)
    image = objectDetect()
    if request.method == 'POST':
        print("hello world")
        data = request.json

        pic_as_base64 = data['pictureString']
        with open("textToDetect.jpg", 'wb') as fh:
            fh.write(base64.b64decode(pic_as_base64))

        imagePath = "./textToDetect.jpg"
        text = image.readText(imagePath)

        os.remove("./textToDetect.jpg")

        log.success()
        return {
            "imageText": text,
        }
    else:
        log.fail(sys.exc_info()[0])
        return "failed check system log"


@application.route('/image', methods=['POST', 'GET'])
def receivePic():
    try:
        log.endpointReached('image', request.remote_addr)
        image = objectDetect()
        print('endpoint reachde')
        if request.method == 'POST':
            print("hello world")
            data = request.json
            pic_as_base64 = data['pictureString']
            with open("imageToDetect.jpg", 'wb') as fh:
                fh.write(base64.b64decode(pic_as_base64))

            imagePath = "./imageToDetect.jpg"
            imageDetected, objectList = image.processImage(imagePath)
            objects = ""
            for i in range(len(objectList)):
                objects += objectList[i] + "\n"

            os.remove("./imageToDetect.jpg")
            os.remove("./imageToDetect_yolo3.jpg")
            log.success()
            return {
                "pictureResponse": imageDetected,
                "objects": objects
            }
        if request.method == 'GET':
            return "<h1>Hello world iSeek</h1>"
    except:
        # log.fail(str(sys.exec_info()))
        return "failed check system log"


@application.route('/recording', methods=['POST', 'GET'])
def receiveWav():
    log.endpointReached('recording', request.remote_addr)
    print('endpoint reached')
    if request.method == 'POST':
        try:
            if 'file' not in request.files:
                log.write("ERROR:\nNo File Given")
                return "no file added"

            wavFile = request.files['file']

            if wavFile.filename != '':
                wavFile.save(wavFile.filename)
            else:
                return "no file name given"

            filePath = "./{}".format(wavFile.filename)
            text = voice.wavToText(filePath)

            os.remove("./{}".format(wavFile.filename))

            jsonText = json.dumps(
                {'textString': text}
            )

            respond = requests.post(url, json=jsonText)

            log.success()

            return {
                "textResponse": respond.text
            }

        except:
            log.fail(str(sys.exec_info()))
            return "failed check system log"

    if request.method == 'GET':
        # return "<h1>Hello world iSeek</h1>"
        return chatbot.chatbot_response('hello')


@application.route('/chatbot', methods=['POST', 'GET'])
def chatbot():
    log.endpointReached('chatbot', request.remote_addr)
    try:
        if request.method == 'POST':
            data = request.json
            message = data['textString']

            jsonText = json.dumps(
                {'textString': message}
            )

            respond = requests.post(url, json=jsonText)
            log.success()
            return {
                "textResponse": respond.text
            }

        if request.method == 'GET':
            return "<h1>Hello world iSeek</h1>"
    except:
        log.fail(str(sys.exec_info()))
        return "failed check system log"


@application.route('/voiceStreamingCheck', methods=['POST', 'GET'])
def voiceMobileNetObjectCheck():
    print("hello there")
    if request.method == 'GET':
        return "<h1>Hello world iSeek</h1>"
    if request.method == 'POST':
        try:
            if 'file' not in request.files:
                log.write("ERROR:\nNo File Given")
                return "no file added"

            wavFile = request.files['file']

            if wavFile.filename != '':
                wavFile.save(wavFile.filename)
            else:
                return "no file name given"

            filePath = "./{}".format(wavFile.filename)
            text = voice.wavToText(filePath)

            print(text)

            os.remove("./{}".format(wavFile.filename))

            keywordCheck = False
            amazonNeeded = False

            for i in range(len(askOptions)):
                if text.lower().startswith(askOptions[i].lower()):
                    keywordCheck = True

            print(text, keywordCheck)

            if text.split() == 1 or keywordCheck:
                availabilityStatus = False
                returnObject = ""
                objectAvailability = ""
                yesNo = False
                if keywordCheck:
                    wantedObject = text.split()[-1]
                else:
                    wantedObject = text

                print(wantedObject)
                for i in range(len(mobileNetOptions)):
                    for j in range(len(mobileNetOptions[i])):
                        if mobileNetOptions[i][j].lower() == wantedObject.lower():
                            availabilityStatus = True
                            yesNo = False
                            if len(mobileNetOptions[i]) == 1:
                                returnObject = mobileNetOptions[i][0]
                                print('x')
                                print(returnObject)
                                return {
                                    "amazonNeeded": amazonNeeded,
                                    "objectAvailability": availabilityStatus,
                                    "yesNoNeeded": yesNo,
                                    "objectChoice": returnObject
                                }
                            else:

                                for k in range(len(mobileNetOptions[i])):
                                # print(mobileNetOptions[i][k])
                                    if k == len(mobileNetOptions[i]) - 1:
                                        returnObject += mobileNetOptions[i][k]
                                    else:
                                        returnObject += mobileNetOptions[i][k] + ", "
                                print('y')
                                print(returnObject)
                                return {
                                    "amazonNeeded": amazonNeeded,
                                    "objectAvailability": availabilityStatus,
                                    "yesNoNeeded": yesNo,
                                    "objectChoice": returnObject
                                }

                if not availabilityStatus:
                    print("here")
                    syns = dictionary.synonym(wantedObject)
                    syns = syns[:2]
                    print(syns)
                    for x in range(len(syns)):
                        for i in range(len(mobileNetOptions)):
                            for j in range(len(mobileNetOptions[i])):
                                if mobileNetOptions[i][j].lower() == syns[x].lower():
                                    yesNo = True
                                    availabilityStatus = True
                                    if len(mobileNetOptions[i]) == 1:
                                        returnObject = mobileNetOptions[i][0]
                                        print('i')
                                        print(returnObject)
                                        return {
                                            "amazonNeeded": amazonNeeded,
                                            "objectAvailability": availabilityStatus,
                                            "yesNoNeeded": yesNo,
                                            "objectChoice": returnObject
                                        }

                                    else:

                                        for k in range(len(mobileNetOptions[i])):
                                            print(mobileNetOptions[i][k])
                                            if k == len(mobileNetOptions[i]) - 1:
                                                returnObject += mobileNetOptions[i][k]
                                            else:
                                                returnObject += mobileNetOptions[i][k] + ", "
                                        print(returnObject)
                                        print('j')

                                        return {
                                            "amazonNeeded": amazonNeeded,
                                            "objectAvailability": availabilityStatus,
                                            "yesNoNeeded": yesNo,
                                            "objectChoice": returnObject
                                        }
                print('k')

                print(returnObject)

                return {
                    "amazonNeeded": amazonNeeded,
                    "objectAvailability": availabilityStatus,
                    "yesNoNeeded": yesNo,
                    "objectChoice": returnObject
                }

            else:
                amazonNeeded = True
                jsonText = json.dumps(
                    {'textString': text}
                )

                respond = requests.post(url, json=jsonText)

                log.success()

                return {
                    "amazonNeeded": amazonNeeded,
                    "textResponse": respond.text
                }





        except Exception as e:
            print(e)
            # log.fail(str(sys.exec_info()))
            return "failed, check system log"


@application.route('/streamingCheck', methods=['POST', 'GET'])
def mobileNetObjectCheck():
    print("reached")
    if request.method == 'GET':
        "<h1>Hello world iSeek</h1>"

    else:

        availabilityStatus = False
        returnObject = ""
        objectAvailability = ""
        yesNo = False
        data = request.json
        wantedObject = data['requestedObject']

        for i in range(len(mobileNetOptions)):
            for j in range(len(mobileNetOptions[i])):
                if mobileNetOptions[i][j].lower() == wantedObject.lower():

                    availabilityStatus = True
                    yesNo = False
                    if len(mobileNetOptions[i]) == 1:
                        returnObject = mobileNetOptions[i][0]
                        break
                    else:

                        for k in range(len(mobileNetOptions[i])):
                            print(mobileNetOptions[i][k])
                            if k == len(mobileNetOptions[i]) - 1:
                                returnObject += mobileNetOptions[i][k]
                            else:
                                returnObject += mobileNetOptions[i][k] + ", "

        if not availabilityStatus:
            print("here")
            syns = dictionary.synonym(wantedObject)

            for x in range(len(syns)):
                for i in range(len(mobileNetOptions)):
                    for j in range(len(mobileNetOptions[i])):
                        if mobileNetOptions[i][j].lower() == syns[x].lower():
                            yesNo = True
                            availabilityStatus = True
                            if len(mobileNetOptions[i]) == 1:
                                returnObject = mobileNetOptions[i][0]
                                return {
                                    "objectAvailability": availabilityStatus,
                                    "yesNoNeeded": yesNo,
                                    "objectChoice": returnObject
                                }

                            else:

                                for k in range(len(mobileNetOptions[i])):
                                    print(mobileNetOptions[i][k])
                                    if k == len(mobileNetOptions[i]) - 1:
                                        returnObject += mobileNetOptions[i][k]
                                    else:
                                        returnObject += mobileNetOptions[i][k] + ", "

                                return {
                                    "objectAvailability": availabilityStatus,
                                    "yesNoNeeded": yesNo,
                                    "objectChoice": returnObject
                                }

        return {
            "objectAvailability": availabilityStatus,
            "yesNoNeeded": yesNo,
            "objectChoice": returnObject
        }


"""
This will be used to gather the possible streaming objects that can be found
using mobilenet
"""
file1 = open('mobileNetObjects.txt', 'r')

while True:
    line = file1.readline().replace("\n", "")
    if not line:
        break
    mobileNetOptions.append(line.split("  ")[1].split(", "))
print(mobileNetOptions)

file2 = open('possibleAsks.txt', 'r')
while True:
    line = file2.readline().replace("\n", "")
    if not line:
        break
    askOptions.append(line)

# for i in range(len(mobileNetOptions)):
#    print(mobileNetOptions[i])
#    for j in range(len(mobileNetOptions[i])):
#        print(mobileNetOptions[i][j])
if __name__ == "__main__":
    application.run(host='0.0.0.0')  # ,threaded=True)
